
import os, random, time, pygame
pygame.init()
SCREEN = (700,450)

DISPLAY = pygame.display.set_mode(SCREEN)


#Definiranje boje, mjere kartice i pravljenje grida
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
ARIAL_200 = pygame.font.SysFont("Arial", 200)
ARIAL_50 = pygame.font.SysFont("Arial", 50)
ARIAL_35 = pygame.font.SysFont("Arial", 35)
ARIAL_20 = pygame.font.SysFont("Arial", 20)
CARD_LEN = 100
CARD_MARGIN = 10
CARD_HOR_PAD = 37
CARD_VER_PAD = 22
ROWS = 4
COLS = 5
cards = [i for i in range(10) for j in range(2)] 
random.shuffle(cards) # niz random brojeva 
CARD_VAL_GRID = [cards[i*len(cards) // ROWS:(i+1)*len(cards) // ROWS] for i in range(ROWS)] #veličina grida
CARD_GRID = [[] for i in range(ROWS)]
for i in range(ROWS):  #napravljen grid za crtanje 
    if i == 0:
        for j in range(COLS):
            if j == 0:
                CARD_GRID[i].append(pygame.Rect(CARD_MARGIN, CARD_MARGIN, CARD_LEN, CARD_LEN))
            else:
                CARD_GRID[i].append(pygame.Rect(CARD_GRID[i][j-1].x + CARD_LEN + CARD_MARGIN, CARD_MARGIN, CARD_LEN, CARD_LEN))
    else:
        for j in range(COLS):
            if j == 0:
                CARD_GRID[i].append(pygame.Rect(CARD_MARGIN, CARD_GRID[i-1][0].y + CARD_LEN + CARD_MARGIN, CARD_LEN, CARD_LEN))
            else:
                CARD_GRID[i].append(pygame.Rect(CARD_GRID[i][j-1].x + CARD_LEN + CARD_MARGIN, CARD_GRID[i-1][0].y + CARD_LEN + CARD_MARGIN, CARD_LEN, CARD_LEN))
global exposed
exposed = []
global matched
matched = []
global wrong
wrong = []
global turns
turns = 0

#Game loop
while True:
    for event in pygame.event.get():
        #Detect quit
        if event.type == pygame.QUIT:
            pygame.quit()

    #Provjera za klik miša
    pressed = list(pygame.mouse.get_pressed())
    for i in range(len(pressed)):
        if pressed[i]:
            for i in range(ROWS):
                for j in range(COLS):
                    mouse_pos = list(pygame.mouse.get_pos()) #dali je miš kliknio karticu 
                    if mouse_pos[0] >= CARD_GRID[i][j].x and mouse_pos[1] >= CARD_GRID[i][j].y and mouse_pos[0] <= CARD_GRID[i][j].x + CARD_LEN and mouse_pos[1] <= CARD_GRID[i][j].y + CARD_LEN:
                        global has_instance
                        has_instance = False
                        for k in range(len(exposed)): #prva kartica
                            if exposed[k] == [i, j]:
                                has_instance = True

                        for k in range(len(matched)): #druga kartica 
                            if matched[k] == [i, j]:
                                has_instance = True

                        if has_instance == False: #otvori 
                            exposed.append([i, j])
                            
    if len(exposed) == 2: #dvije otvorene 
        turns += 1
        if CARD_VAL_GRID[exposed[0][0]][exposed[0][1]] == CARD_VAL_GRID[exposed[1][0]][exposed[1][1]]: #točne dvije 
            matched.extend(exposed)
            exposed.clear()
            
        else:#krive
            wrong.extend(exposed)
            exposed.clear()

    #Clearati screen
    DISPLAY.fill(BLACK)

    #Prikaz za sve kartice u gridu 
    for i in range(ROWS):
        for j in range(COLS):
            pygame.draw.rect(DISPLAY, (255, 255, 255), CARD_GRID[i][j])
            
    #Prikaz brojeva otvorena kartica 
    if exposed:
        for i in exposed:
            text = str(CARD_VAL_GRID[i[0]][i[1]])
            render = ARIAL_50.render(text, True, BLACK)
            DISPLAY.blit(render, (CARD_GRID[i[0]][i[1]].x + CARD_HOR_PAD, CARD_GRID[i[0]][i[1]].y + CARD_VER_PAD))

    if matched: # pogođene kartice 
        for i in matched:
            text = str(CARD_VAL_GRID[i[0]][i[1]])
            render = ARIAL_50.render(text, True, GREEN)
            DISPLAY.blit(render, (CARD_GRID[i[0]][i[1]].x + CARD_HOR_PAD, CARD_GRID[i[0]][i[1]].y + CARD_VER_PAD))

    if wrong: #krive kartice
        for i in wrong:
            text = str(CARD_VAL_GRID[i[0]][i[1]])
            render = ARIAL_50.render(text, True, RED)
            DISPLAY.blit(render, (CARD_GRID[i[0]][i[1]].x + CARD_HOR_PAD, CARD_GRID[i[0]][i[1]].y + CARD_VER_PAD))

    #Prikaz ostalih stvari
    title = ARIAL_35.render("Memory", True, WHITE)
    DISPLAY.blit(title, (570, 10))
    turn_text = ARIAL_20.render("Turns: " + str(turns), True, WHITE)
    DISPLAY.blit(turn_text, (580, 75))

    #Provjera za pobjedu
    if len(matched) == 20:
        DISPLAY.fill(BLACK)
        win = ARIAL_200.render("You win!", True, GREEN)
        DISPLAY.blit(win, (40, 105))
        pygame.display.flip()
        break
    
    pygame.display.flip()
    if wrong:
        time.sleep(1)
        wrong.clear()
