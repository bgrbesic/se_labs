# ukljucivanje biblioteke pygame
import pygame
 
def next_bg(color): 
    if color == TIRKIZ:
        color = RED
    elif color == RED:
        color = MAGENTA
    elif color == MAGENTA:
        color = BLUE
    elif color == BLUE:
        color = TIRKIZ      
    return color 


def next_sl(slika):
    if slika == bg_img:
        slika = bg_img1
    elif slika == bg_img1:
        slika = bg_img2
    elif slika == bg_img2:
        slika = bg_img    
    return slika                

pygame.init()
# definiranje konstanti za velicinu prozora
WIDTH = 1600
HEIGHT = 900

RED= [255, 0 ,0]
GREEN = [0, 255, 0]
BLUE = [0, 0, 255]
TIRKIZ = [64, 224, 208]
MAGENTA = [255, 0, 255]

bg_img = pygame.image.load("bg.jpg")
bg_img = pygame.transform.scale(bg_img, (WIDTH-100, HEIGHT-100))
bg_img1 = pygame.image.load("bg1.jpeg")
bg_img1 = pygame.transform.scale(bg_img1, (WIDTH-100, HEIGHT-100))
bg_img2 = pygame.image.load("bg3.jpeg")
bg_img2 = pygame.transform.scale(bg_img2, (WIDTH-100, HEIGHT-100))

# tuple velicine prozora
size = (WIDTH, HEIGHT)
 
#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)
#definiranje naziva prozora
pygame.display.set_caption("Nasa kul igra")
 
clock = pygame.time.Clock()
i = 0 
duration = 1000
bg_color = TIRKIZ
bg_slika = bg_img
done = False
while not done:
    #event petlja
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                bg_color = next_bg(bg_color) 
                bg_slika = next_sl(bg_slika)
                                         
    i += 1
    if duration < 0:
        duration = 1000
        bg_color = next_bg(bg_color)
        bg_slika = next_sl(bg_slika)
    
    screen.fill( bg_color )
    screen.blit( bg_slika ,(0,0))
    
    
    pygame.display.flip()
   
    #ukoliko je potrebno ceka do iscrtavanja
    #iduceg framea kako bi imao 60fpsa
    time = clock.tick(60)
    duration = duration - time
    #print(duration)
pygame.quit()